package main

import (
	"io/ioutil"
	"net/http"
	"os"
	"strings"

	"github.com/gin-gonic/gin"
	"golang.org/x/text/encoding/charmap"
)

type decoded struct {
	Url  string `json:"url"`
	Text string `json:"text"`
}

const ENCODED_DIR string = "encoded/"
const DECODED_DIR string = "decoded/"

func textByName(c *gin.Context) {
	name := c.DefaultQuery("filename", "")
	response := decodedResponse(name)
	c.IndentedJSON(http.StatusOK, response)
}

func uploadAndDecode(c *gin.Context) {
	file, _ := c.FormFile("file")
	filename := strings.ToLower(file.Filename)
	path := ENCODED_DIR + filename
	c.SaveUploadedFile(file, path)
	decode(filename)
	response := decodedResponse(filename)
	c.IndentedJSON(http.StatusOK, response)
}

func decode(name string) {
	filepath := ENCODED_DIR + name
	file, err := os.Open(filepath)
	if err != nil {
		panic(err)
	}

	defer file.Close()
	decorder := charmap.Windows1251.NewDecoder()
	reader := decorder.Reader(file)
	buf, err := ioutil.ReadAll(reader)
	if err != nil {
		panic(err)
	}

	out := DECODED_DIR + name
	err = ioutil.WriteFile(out, buf, 0644)
	if err != nil {
		panic(err)
	}
}

func readByName(name string) string {
	buf, err := ioutil.ReadFile(DECODED_DIR + name)
	if err != nil {
		panic(err)
	}
	return string(buf)
}

func decodedResponse(name string) decoded {
	return decoded{
		Url:  DECODED_DIR + name,
		Text: readByName(name),
	}
}

func main() {
	router := gin.Default()
	router.MaxMultipartMemory = 8 << 20 // 8 MiB
	router.GET("/decode", textByName)
	router.POST("/decode", uploadAndDecode)

	router.Run(":1342")
}
